# Simple Flask Object Wrappers #
Python module to add simple flask object wrappers for the following flask objects:
* ```class flask.Flask```
* ```class flask.Blueprint```

## Examples ##

```python
# Flask application wrapper
from flask_wrappers import FlaskAppWrapper

# Example module where route callbacks are defined
import example_module

# Global routes map
routes = {
    '/': 'root',
    '/home': 'home',
    '/login': 'login',
}

def main(*args, **kwargs):
    app = FlaskAppWrapper()
    app.add_routes(example_module, routes)
    app.run()

if __name__ == "__main__":
    main()
```

## Dependencies ##
* Flask (Version 1.1.1)