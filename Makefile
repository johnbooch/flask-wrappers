PYTHON=python3.7
ENV_PIP=env/bin/pip3.7
ENV_PYTHON=env/bin/python3.7

.PHONY: setup-env server

setup-env: ## Setup development environment
	# Clean virtual env directory
	if [ -d env ]; then rm -rf env; fi;
	mkdir env
	# Create virtual env
	$(PYTHON) -m virtualenv env/
	# Install dependencies 
	$(ENV_PIP) install -r requirements.txt

server:
	./env/bin/python3.7 examples/appwrap_ex.py
