from flask import Flask
from flask import Blueprint

class FlaskAppWrapper:
    def __init__(self, app=None):
        if app:
            self.app = app
        else:
            self.app = Flask(__name__)

    def add_routes(self, module, routes):
        for route, method in routes.items():
            func = getattr(module, method)
            self.app.add_url_rule(route, func.__name__, func)

    def run(self):
        self.app.run()

class FlaskBlueprintWrapper:
    def __init__(self, blueprint=None, *args, **kwargs):
        if type(blueprint) is Blueprint:
            self.blueprint = blueprint
        elif isinstance(blueprint, Blueprint):
            self.blueprint = Blueprint(blueprint,
                __name__, *args, **kwargs)
        else:
            self.blueprint = Blueprint('wrapped_blueprint',
                __name__, *args, **kwargs)

    def add_routes(self, module, routes):
        for route, method in routes.items():
            func = getattr(module, method)
            self.blueprint.add_url_rule(route, func.__name__, func)